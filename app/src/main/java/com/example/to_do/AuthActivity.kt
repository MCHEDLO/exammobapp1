package com.example.to_do

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_signupactivity.*

class AuthActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.colorName)
        }


    }


    private fun init() {
        auth = Firebase.auth
        signinbutton.setOnClickListener {
            sigin()


        }
        signupbutton.setOnClickListener {
            val intent = Intent(this, Signupactivity::class.java)
            startActivity(intent)
        }


    }

    private fun sigin() {
        val email = edittextemail.text.toString()
        val password = edittextpassword.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()) {
            signinbutton.isClickable = false

            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    signinbutton.isClickable = true
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        d("Signin", "signInWithEmail:success")
                        val user = auth.currentUser
                        Toast.makeText(
                            baseContext,
                            "Authentication is Success!",
                            Toast.LENGTH_SHORT
                        ).show()
                        val intent = Intent(this, Todo::class.java)
                        startActivity(intent)
                    } else {
                        // If sign in fails, display a message to the user.
                        d("Signin", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Authentication failed",
                            Toast.LENGTH_SHORT
                        ).show()


                    }

                }

        } else {
            Toast.makeText(this, "Please fill all Fields", Toast.LENGTH_SHORT).show()
        }
    }

}
