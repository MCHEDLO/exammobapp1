package com.example.to_do

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_signupactivity.*

class Signupactivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signupactivity)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signupbutton1.setOnClickListener {
            signup()


        }
    }

    private fun emailcheck() {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(emailsignup.text.toString()).matches()) {

        } else {
            emailsignup.setError("Email format is not correct")
        }
    }

    private fun signup() {
        emailcheck()
        val repeatpassword = repeatpasswordedittext.text.toString()
        val password = passwordsignup.text.toString()
        val email = emailsignup.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty() && repeatpassword.isNotEmpty()) {
            if (password == repeatpassword) {
                signupbutton1.isClickable = false
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        signupbutton1.isClickable = true

                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("Signup", "createUserWithEmail:success")
                            val user = auth.currentUser
                            Toast.makeText(this, "Sign up is successful", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, AuthActivity::class.java)
                            startActivity(intent)
                        } else {
                            // If sign in fails, display a message to the user.
                            d("Signup", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()

                        }


                    }


            } else {
                Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }

    }


}